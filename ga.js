let ga = (function() {
    let WIDTH = null;
    let HEIGHT = null;
    let X_WIDTH_BASE = 100;
    let Y_HEIGHT_BASE = 100;
    
    let zoom = 1;
    let dens = 10;
    
    let style = {
        backgroundColor: "#eeeeee", 
        gridColor: "#bbbbbb", 
        scaleColor: "#000000", 
        scaleFont: "12px Arial"
    };
    
    let ctx = null;
    
    let init = function(c) {
        WIDTH = c.width;
        HEIGHT = c.height;
        ctx = c.getContext("2d");
        doZoom(0);
    };
    
    let reset = function() {
        ctx.fillStyle = style.backgroundColor;
        ctx.fillRect(0,0,WIDTH,HEIGHT);
    
        // grid
        for (x = 0; x < Math.ceil(WIDTH/x_width_zoom); x++) {
            ctx.beginPath();
            ctx.moveTo(x*x_width_zoom, 0);
            ctx.lineTo(x*x_width_zoom, (max_y+1)*y_height_zoom);
            ctx.strokeStyle=style.gridColor;
            ctx.stroke();
        }
        for (y = 0; y < Math.ceil(HEIGHT/y_height_zoom); y++) {
            ctx.beginPath();
            ctx.moveTo(0, y*y_height_zoom);
            ctx.lineTo((max_x+1)*x_width_zoom, y*y_height_zoom);
            ctx.strokeStyle=style.gridColor;
            ctx.stroke();
        }
        
        // escala
        for (x = 0; x < Math.ceil(WIDTH/x_width_zoom); x++) {
            ctx.beginPath();
            ctx.moveTo(x*x_width_zoom, 0);
            ctx.lineTo(x*x_width_zoom, 12);
            ctx.strokeStyle=style.scaleColor;
            ctx.stroke();
            ctx.font=style.scaleFont;
            ctx.fillStyle=style.scaleColor;
            ctx.fillText(x, x*x_width_zoom, 26, x_width_zoom-10);
        }
        for (y = 0; y < Math.ceil(HEIGHT/y_height_zoom); y++) {
            ctx.beginPath();
            ctx.moveTo(0, y*y_height_zoom);
            ctx.lineTo(12, y*y_height_zoom);
            ctx.strokeStyle="#000000";
            ctx.stroke();
            ctx.font=style.scaleFont;
            ctx.fillStyle=style.scaleColor;
            ctx.fillText(y, 16, y*y_height_zoom, y_height_zoom-10);
        }
    };
    
    let doZoom = function(s) {
        if (s == 0) zoom = 1;
        else {
            if (!(zoom+s/10<0.2||zoom+s/10>1.9))
                zoom+=s/10;
        }
        x_width_zoom = X_WIDTH_BASE*zoom;
        y_height_zoom = Y_HEIGHT_BASE*zoom;
        max_x = Math.floor(WIDTH/x_width_zoom);
        max_y = Math.floor(HEIGHT/y_height_zoom);
        redoAll();
    };
    
    let redoAll = function() {
        reset();
        for (let i = 0; i < hist.length; i++) 
            hist[i].f.apply(this, hist[i].a);
    };
    
    let hist = [];
    
    // funções
    let ponto = function(x, y, thickness, color) {
        let xp = x*x_width_zoom;
        let yp = y*y_height_zoom;
        ctx.beginPath();
        ctx.arc(xp, yp, thickness, 0, 2 * Math.PI, true);
        ctx.fillStyle = color;
        ctx.fill();
    };
    
    let segmento = function(p1x, p1y, p2x, p2y, thickness, color, showCoords) {
        let p1xp = p1x*x_width_zoom;
        let p1yp = p1y*y_height_zoom;
        let p2xp = p2x*x_width_zoom;
        let p2yp = p2y*y_height_zoom;
        
        let hdist = p2x - p1x;
        let vdist = p2y - p1y;
        let dist = Math.sqrt(Math.pow(hdist, 2)+Math.pow(vdist, 2));
        
        let steps = Math.ceil(dist*(dens/100)*x_width_zoom); // poderia ser y_height_zoom? produto dos 2?
        for (let i = 0; i < steps; i ++) {
            let hp = hdist/steps*i;
            let vp = vdist/steps*i;
            ponto(p1x+hp, p1y+vp, thickness, color);
        }
        
        ponto(p1x, p1y);
        ponto(p2x, p2y);
        if (showCoords) {
            ctx.font = "14px Arial";
            ctx.fillText("("+p1x+", "+p1y+")", p1xp, p1yp-5);
            ctx.fillText("("+p2x+", "+p2y+")", p2xp, p2yp+13);
        }
    };
    
    let retaColinearidade = function(p1x, p1y, p2x, p2y) {
        // dados dois pontos, sabendo que esses dois e os demais pontos 
        // n da reta devem ser colineares, montamos a equação de colinearidade 
        // de três pontos e isolamos uma das coordenadas do ponto desconhecido. 
        // aplicamos a equação para as coordenadas X dos pontos desconhecidos.
        let a = p1y-p2y;
        let b = p2x-p1x;
        let c = (p1x*p2y)-(p2x*p1y);
        
        let seg = {};
        let execucoes = max_x+2;
        for (let x = 0; x < execucoes; x++) {
            let y = (0-(a*x)-c)/b;
            if (x == 0) seg.a = {x,y};
            else if (x == execucoes-1) seg.b = {x,y};
        }
        segmento(seg.a.x, seg.a.y, seg.b.x, seg.b.y, 2, "yellow");
        ctx.font = "14px Arial";
        ctx.fillText("("+p1x+", "+p1y+")", (p1x*x_width_zoom)+10, p1y*y_height_zoom, 90);
        ctx.fillText("("+p2x+", "+p2y+")", (p2x*x_width_zoom)+10, p2y*y_height_zoom, 90);
    };
    
    let retaCoangularidade = function(px, py, a) {
        // dado um ponto e seu ângulo para o eixo x, montamos um triângulo 
        // reto e usamos a razão entre os catetos (tangente) para calcular onde 
        // a reta passa pelo eixo Y (n). com esse valor, para demais pontos, 
        // aplicamos a mesma tangente para determinar Y em função de X e de n.
        let arad = Math.PI*a/180;
        let n = (px*-1*Math.tan(arad))+py;

        let seg = {};
        let execucoes = max_x+2;
        for (let x = 0; x < execucoes; x++) {
            let y = (x*Math.tan(arad))+n;
            if (x == 0) seg.a = {x,y};
            else if (x == execucoes-1) seg.b = {x,y};
        }
        segmento(seg.a.x, seg.a.y, seg.b.x, seg.b.y, 2, "orange");
        ctx.font = "14px Arial";
        ctx.fillText("("+px+", "+py+")", (px*x_width_zoom)+10, py*y_height_zoom, 90);
    };
    
    return {
        init: init, 
        zoom: doZoom, 
        dens: function(d) {dens = d; redoAll();}, 
        getZoom: function() {return zoom;}, 
        ponto: function(args) {hist.push({f: ponto, a: args}); ponto.apply(this, args);}, 
        segmento: function(args) {hist.push({f: segmento, a: args}); segmento.apply(this, args);}, 
        retaColinearidade: function(args) {hist.push({f: retaColinearidade, a: args}); retaColinearidade.apply(this, args);}, 
        retaCoangularidade: function(args) {hist.push({f: retaCoangularidade, a: args}); retaCoangularidade.apply(this, args);}
    };
})();